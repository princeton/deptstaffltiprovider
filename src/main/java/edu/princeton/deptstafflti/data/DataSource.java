package edu.princeton.deptstafflti.data;

public class DataSource {
    private String batchuid;
    private String description;
   
    
    public String getBatchuid() {
    	return batchuid;
    }
    public void setBatchuid(String id) {
    	batchuid = id;
    }
    public String getDescription() {
    	return description;
    }
    public void setDescription(String d) {
    	description = d;
    }
   
}