package edu.princeton.deptstafflti.data;

public class User {
    private String puid;
    private String family;
    private String given;
    private String netid;
    private String uuid;
    
    public String getPuid() {
    	return puid;
    }
    public void setPuid(String id) {
    	puid = id;
    }
    public String getFamily() {
    	return family;
    }
    public void setFamily(String last) {
    	family = last;
    }
    public String getGiven() {
    	return given;
    }
    public void setGiven(String first) {
    	given = first;
    }
    public String getNetid() {
    	return netid;
    }
    public void setNetid(String netid){
    	this.netid = netid;
    }
    public String getUuid() {
    	return uuid;
    }
    public void setUuid(String uid){
    	this.uuid = uid;
    }
}