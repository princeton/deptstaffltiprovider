package edu.princeton.deptstafflti.data;

/**
 * This class represents a CourseAdmin record.
 */
public class CourseAdmin {
	public String term 				= "";
	public String dept 				= "";
	public String netid 			= "";
	public String emplid 			= "";
	public String career 			= "";
	public String role 				= "";
	public String crosslisted		= "";
	
	public CourseAdmin(CourseAdmin admin) {
		super();
		this.term = admin.term;
		this.dept = admin.dept;
		this.netid = admin.netid;
		this.emplid = admin.emplid;
		this.career = admin.career;
		this.role = admin.role;
		this.crosslisted = admin.crosslisted;
		
	}
	public String getCareer() {
		return career;
	}
	public void setCareer(String career) {
		this.career = career;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getEmplid() {
		return emplid;
	}
	public void setEmplid(String emplid) {
		this.emplid = emplid;
	}
	public String getNetid() {
		return netid;
	}
	public void setNetid(String netid) {
		this.netid = netid;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String t) {
		this.term = t;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getCrosslisted() {
		return crosslisted;
	}
	public void setCrosslisted(String crosslisted) {
		this.crosslisted = crosslisted;
	}

	/**
	 * @return
	 */
	public String getDescription() {
		StringBuffer sb = new StringBuffer(); 
		sb.append("Department = " + dept + ", ");
		sb.append("Semester = " + term + ", ");
		sb.append("NetID=" + netid + ", ");
		sb.append("emplid=" + emplid + ", ");
		sb.append("Courses = " + career + ", ");
		sb.append("Role = " + role + ", ");
		sb.append("Cross-listed = " + crosslisted);
		return sb.toString();
	} 
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer(); 
		sb.append("dept=" + dept + ", ");
		sb.append("term=" + term + ", ");
		sb.append("netid=" + netid + ", ");
		sb.append("emplid=" + emplid + ", ");
		sb.append("career=" + career + ", ");
		sb.append("role=" + role + ", ");
		sb.append("crosslisted=" + crosslisted + ", ");
		
		return sb.toString();
	}
}
