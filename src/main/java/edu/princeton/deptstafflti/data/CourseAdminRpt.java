package edu.princeton.deptstafflti.data;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a CourseAdmin record.
 */
public class CourseAdminRpt {
	public String term 				= "";
	public String dept 				= "";
	public String netid 			= "";
	public String emplid 			= "";
	public String career 			= "";
	public String role 				= "";
	public String crosslisted		= "";
	public List<String> successes	= new ArrayList<String>();
	public List<String> errors		= new ArrayList<String>();
	
	public CourseAdminRpt(CourseAdmin courseAdmin) {
		this.term = courseAdmin.term;
		this.dept = courseAdmin.dept;
		this.netid = courseAdmin.netid;
		this.emplid = courseAdmin.emplid;
		this.career = courseAdmin.career;
		this.role = courseAdmin.role;
		this.crosslisted = courseAdmin.crosslisted;
	}
	
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	
	public String getNetid() {
		return netid;
	}
	public void setNetid(String netid) {
		this.netid = netid;
	}
	public String getEmplid() {
		return emplid;
	}
	public void setEmplid(String emplid) {
		this.emplid = emplid;
	}
	public String getCareer() {
		return career;
	}
	public void setCareer(String career) {
		this.career = career;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getCrosslisted() {
		return crosslisted;
	}
	public void setCrosslisted(String crosslisted) {
		this.crosslisted = crosslisted;
	}

	public String getDescription() {
		StringBuffer sb = new StringBuffer(); 
		sb.append("Department = " + dept + ", ");
		sb.append("Semester = " + term + ", ");
		sb.append("NetID=" + netid + ", ");
		sb.append("emplid=" + emplid + ", ");
		sb.append("Courses = " + career + ", ");
		sb.append("Role = " + role + ", ");
		sb.append("Cross-listed = " + crosslisted);
		return sb.toString();
	} 
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer(); 
		sb.append("dept=" + dept + ", ");
		sb.append("term=" + term + ", ");
		sb.append("netid=" + netid + ", ");
		sb.append("emplid=" + emplid + ", ");
		sb.append("career=" + career + ", ");
		sb.append("role=" + role + ", ");
		sb.append("crosslisted=" + crosslisted + ", ");
		return sb.toString();
	}
	public void addSuccess(String success) {
		successes.add(success);		
	}
	public void addError(String error) {
		errors.add(error);
	}
	public List<String> getSuccesses() {
		return successes;
	}
	public List<String> getErrors() {
		return errors;
	}
}
