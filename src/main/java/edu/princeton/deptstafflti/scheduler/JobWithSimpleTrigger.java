package edu.princeton.deptstafflti.scheduler;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;
import org.springframework.stereotype.Component;

import edu.princeton.deptstafflti.config.DeptStaffProperties;
import edu.princeton.deptstafflti.model.SettingsModel;
import edu.princeton.deptstafflti.service.DeptStaffRepository;
import edu.princeton.deptstafflti.service.SettingsService;
import edu.princeton.deptstafflti.service.UpdateEnrollmentService;
 
@Component
@DisallowConcurrentExecution
public class JobWithSimpleTrigger implements Job {
	
	@Autowired
	SettingsService s;
	
	@Autowired
	DeptStaffRepository dsRepos;
	
	@Override
	public void execute(JobExecutionContext jobExecutionContext) {
		
		UpdateEnrollmentService service = new UpdateEnrollmentService();
		service.update(s);
	}
	
	@Bean(name = "jobBean")
    public JobDetailFactoryBean job() {
        return ConfigureQuartz.createJobDetail(this.getClass());
    }
 
    @Bean(name = "simpleTrigger")
    public SimpleTriggerFactoryBean jobTrigger(@Qualifier("jobBean") JobDetail jobDetail) {
    	//default value
    	long p  = 3000000L;
    	long frequency = 0L;
    	SettingsModel m = s.loadSettings();
    	m = s.loadSettings();
    
    	if (m.getSchedulerPeriod() != null){
    		frequency = Long.parseLong(m.getSchedulerPeriod()) == 0 ? 
    				p:Long.parseLong(m.getSchedulerPeriod()) * 1000;
    	}
    	return ConfigureQuartz.createTrigger(jobDetail, frequency);
    	
    	//return ConfigureQuartz.createTrigger(jobDetail, p);
    }
}