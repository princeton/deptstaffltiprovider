package edu.princeton.deptstafflti.scheduler;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import edu.princeton.deptstafflti.model.SettingsModel;
import edu.princeton.deptstafflti.service.SettingsService;

@Component
@RefreshScope
public class Emailer
{
    private Logger log = LoggerFactory.getLogger(getClass());
    
    @Autowired
	SettingsService settingsService;
    /**
     * @param msg
     * @param emailTo
     * @return
     */
    public void sendEmail(
        String emailFrom,
        String emailSubject,
        String emailText,
        String emailTo)
    {
        try {
            Properties properties = new Properties();
            SettingsModel settingsModel = settingsService.loadSettings();
            //properties.put("mail.smtp.host", settingsModel.getEmailHost());
            Session session = Session.getDefaultInstance(properties, null);

            // create a new MimeMessage object (using the Session created above)
            Message message = new MimeMessage(session);
            
            InternetAddress from = new InternetAddress(emailFrom);
            InternetAddress to = new InternetAddress(emailTo);
            
            message.setFrom(from);

            InternetAddress[] toInternetAddresses = {to};

            message.setRecipients(Message.RecipientType.TO, toInternetAddresses);
            
            message.setSubject(emailSubject);
            message.setText(emailText);
            
            Transport.send(message);

            log.debug("message sent");
        } catch (Exception e) {
            log.error(e.toString());
        }
    }
}