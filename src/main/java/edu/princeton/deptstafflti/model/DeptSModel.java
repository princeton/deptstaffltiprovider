package edu.princeton.deptstafflti.model;

import edu.princeton.deptstafflti.data.Subject;
import edu.princeton.deptstafflti.data.Term;
import edu.princeton.deptstafflti.entity.Career;
import edu.princeton.deptstafflti.entity.Crosslisted;
import edu.princeton.deptstafflti.entity.Role;
import edu.princeton.deptstafflti.service.ValidNetid;

public class DeptSModel {
	
	public String caller;
	public Career career = Career.ALL;
	public Crosslisted crosslisted = Crosslisted.YES;
	public Term currentTerm;
	public Subject currentSubject;
	public String isAdmin;
	@ValidNetid(message="The user must exist in Blackboard, is it the correct netid for the person you want to enroll?")
	public String netids;
	public Role role = Role.INSTRUCTIONAL_STAFF;
	
	public String getCaller() {
		return caller;
	}
	public void setCaller(String c){
		this.caller = c;
	}
	public String getIsAdmin() {
		return isAdmin;
	}
	public void setIsAdmin(String s) {
		this.isAdmin = s;
	}
	public String getNetids() {
		return netids;
	}
	public void setNetids(String ids) {
		this.netids = ids;
	}
	public Term getCurrentTerm() {
		return currentTerm;
	}
	public void setCurrentTerm(Term term) {
		this.currentTerm = term;
	}
	public Subject getCurrentSubject() {
		return currentSubject;
	}
	public void setCurrentSubject(Subject s) {
		this.currentSubject = s;
	}
	public Career getCareer() {
		return career;
	}
	public void setCareer(Career c) {
		this.career = c;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role r) {
		this.role = r;
	}
	public Crosslisted getCrosslisted() {
		return crosslisted;
	}
	public void setCrosslisted(Crosslisted c) {
		this.crosslisted = c;
	}
}
