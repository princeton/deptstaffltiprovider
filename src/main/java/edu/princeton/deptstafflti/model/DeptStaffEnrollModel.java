package edu.princeton.deptstafflti.model;

public class DeptStaffEnrollModel {
	public String netid;
	public String role;
	public String career;
	public String crosslisted;
	public String name;
	
	public String getNetid() {
		return netid;
	}
	public void setNetid(String id) {
		this.netid = id;
	}
	public String getCareer() {
		return career;
	}
	public void setCareer(String c) {
		this.career = c;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String r) {
		this.role = r;
	}
	public String getCrosslisted() {
		return crosslisted;
	}
	public void setCrosslisted(String c) {
		this.crosslisted = c;
	}
	public String getName() {
		return name;
	}
	public void setName(String name){
		this.name = name;
	}
}
