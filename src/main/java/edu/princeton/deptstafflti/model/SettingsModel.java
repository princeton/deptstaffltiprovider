package edu.princeton.deptstafflti.model;

public class SettingsModel {
	private String learnUrl;
    private String key;
    private String restKey;
    private String secret;
    private String restSecret;
    private String authEndpoint;
    private String authAction;
    private String authMethod;
    private String authParamId;
    private String authParamOrg;
	private String emailTo;
	private String emailFrom;
	private String scheduleStart;
	private String schedulerPeriod;
	private String lastUpdated;
	private String webUrl;
	private String webSvcKey;
	private String currentSemester;
	
	public String getLearnUrl() {
        return learnUrl;
    }

    public void setLearnUrl(String learnUrl) {
        this.learnUrl = learnUrl;
    }
    
	public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
    
    public String getRestKey() {
        return restKey;
    }

    public void setRestKey(String key) {
        this.restKey = key;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
    
    public String getRestSecret() {
        return restSecret;
    }

    public void setRestSecret(String secret) {
        this.restSecret = secret;
    }
    
    public String getAuthEndpoint() {
        return authEndpoint;
    }

    public void setAuthEndpoint(String endpoint) {
        this.authEndpoint = endpoint;
    }
    public String getAuthAction() {
        return authAction;
    }

    public void setAuthAction(String action) {
        this.authAction = action;
    }
    public String getAuthMethod() {
        return authMethod;
    }

    public void setAuthMethod(String method) {
        this.authMethod = method;
    }
    public String getAuthParamId() {
        return authParamId;
    }

    public void setAuthParamId(String id) {
        this.authParamId = id;
    }
    public String getAuthParamOrg() {
        return authParamOrg;
    }

    public void setAuthParamOrg(String paramOrg) {
        this.authParamOrg = paramOrg;
    }

	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}
	
	
	public String getEmailFrom() {
		return emailFrom;
	}

	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}
    
	public void setScheduleStart(String f){
		this.scheduleStart = f;
	}

	public String getScheduleStart() {
		return scheduleStart;
	}

	public String getSchedulerPeriod() {
		return schedulerPeriod;
	}

	public void setSchedulerPeriod(String p) {
		this.schedulerPeriod = p;
	}
	
	public void setLastUpdated(String p) {
		this.lastUpdated = p;	
	}
	
	public String getLastUpdated() {
		return this.lastUpdated;
	}
	
	public String getWebUrl() {
		return this.webUrl;
	}
	public void setWebUrl(String webUrl) {
		this.webUrl = webUrl;
	}
	
	public String getWebSvcKey() {
		return this.webSvcKey;
	}
	public void setWebSvcKey(String key) {
		this.webSvcKey = key;
	}
	public String getCurrentSemester() {
		return this.currentSemester;
	}
	public void setCurrentSemester(String semester) {
		this.currentSemester = semester;
	}
	
	
	public boolean equals(SettingsModel settings){
		
		if(settings != null){
		return(!(this.learnUrl.equals(settings.getLearnUrl()) &&
				this.key.equals(settings.getKey()) &&
				this.restKey.equals(settings.getRestKey()) &&
				this.restSecret.equals(settings.getRestSecret()) &&
				this.authEndpoint.equals(settings.getAuthEndpoint()) &&
				this.authAction.equals(settings.getAuthAction()) &&
				this.authMethod.equals(settings.getAuthMethod()) &&
				this.authParamId.equals(settings.getAuthParamId()) &&
				this.authParamOrg.equals(settings.getAuthParamOrg()) &&
				//this.photosDir.equals(settings.getPhotosDir()) &&
				//this.emailServerName.equals(settings.getEmailServerName()) &&
				this.emailTo.equals(settings.getEmailTo()) &&
				this.emailFrom.equals(settings.getEmailFrom()) &&
				this.scheduleStart.equals(settings.getScheduleStart()) &&
				this.schedulerPeriod.equals(settings.getSchedulerPeriod()) &&
				this.webUrl.equals(settings.getWebUrl()) &&
				this.webSvcKey.equals(settings.getWebSvcKey()) &&
				this.currentSemester.equals(settings.getCurrentSemester())));
		}
		return false;
	}
}

