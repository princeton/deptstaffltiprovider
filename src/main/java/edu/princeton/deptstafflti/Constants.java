package edu.princeton.deptstafflti;

public class Constants {
	public static final String SCHEDULER_PERIOD_DEFAULT		= "timeInterval";
	public static final String SCHEDULE_START_DEFAULT  		= "startHour";
	public static final String PROPERTY_FILE_ABSOLUTE_PATH 	= "/source/tmp/deptstafflti.properties";
	public static final String INSTRUCTOR_ROLE 				= "Instructor";
	public static final String ADMINISTRATOR_ROLE 			= "Administrator";
	public final static String AUTH_PATH 					= "/learn/api/public/v1/oauth2/token";
	public final static String COURSES_PATH 				= "/learn/api/public/v1/courses";
	public final static String USERS_PATH 					= "/learn/api/public/v1/users";
	public final static String DS_BATCH_UID 				= "pu-deptstaff";
	public final static String DS_DESCRIPTION 				= "Enrollments created using the pu-deptstafflti plugin.";
	public static final String DATASOURCE_PATH 				= "/learn/api/public/v1/dataSources";
	public static final String SUCCESS 						= "successed";
	public static final String FAIL 						= "failed";
	public static final String COURSEMEMBERSHIP_PATH 		= "/learn/api/public/v1/courses" ;
	public static final String INSTRUCTIONAL_STAFF_ROLE 	= "Instructional Staff";
	public static final String COURSE_BUILDER_ROLE 			= "Course Builder";

	
}