package edu.princeton.deptstafflti.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.context.PropertyPlaceholderAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.autoconfigure.RefreshAutoConfiguration;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import edu.princeton.deptstafflti.exception.GenericLtiException;
import edu.princeton.deptstafflti.model.SettingsModel;
import edu.princeton.deptstafflti.service.impl.SettingsServiceImpl;

@Configuration 
@EnableConfigurationProperties(DeptStaffProperties.class) 
@Import({ RefreshAutoConfiguration.class, PropertyPlaceholderAutoConfiguration.class }) 
public class DeptStaffConfiguration {
	
	@Autowired
	private DeptStaffProperties properties;
	
	@Bean
	@RefreshScope
	public SettingsServiceImpl service() {
		SettingsServiceImpl sservice = new SettingsServiceImpl();
		SettingsModel s = new SettingsModel();
		s.setLearnUrl(properties.getLearnUrl());
		s.setKey(properties.getKey());
		s.setRestKey(properties.getRestKey());
		s.setSecret(properties.getSecret());
		s.setRestSecret(properties.getRestSecret());
		s.setAuthEndpoint(properties.getAuthEndpoint());
		s.setAuthMethod(properties.getAuthMethod());
		s.setAuthAction(properties.getAuthAction());
		s.setAuthParamId(properties.getAuthParamId());
		s.setAuthParamOrg(properties.getAuthParamOrg());
		s.setEmailTo(properties.getEmailTo());
		s.setEmailFrom(properties.getEmailFrom());
		s.setScheduleStart(properties.getScheduleStart());
		s.setSchedulerPeriod(properties.getSchedulerPeriod());
		s.setLastUpdated(properties.getLastUpdated());
		s.setWebUrl(properties.getWebUrl());
		s.setWebSvcKey(properties.getWebSvcKey());
		s.setCurrentSemester(properties.getCurrentSemester());
		sservice.setSettingsModel(s);
		return sservice;
	}
}