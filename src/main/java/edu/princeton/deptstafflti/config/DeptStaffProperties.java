package edu.princeton.deptstafflti.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

import edu.princeton.deptstafflti.model.SettingsModel;


@ConfigurationProperties 
@ManagedResource
@RefreshScope
public class DeptStaffProperties {
	
	@Value("${learnUrl:http://localhost:9876}")
    private String learnUrl;

	@Value("${key:keyDeptStaff}") 
    private String key;
	
	@Value("${restKey}") 
    private String restKey;
	
	@Value("${secret:secretDeptStaff}")
    private String secret;
	
	@Value("${restSecret}")
    private String restSecret;
	
	@Value("${authEndpoint}")
	private String authEndpoint;
	
	@Value("${authAction}")
    private String authAction;
	
	@Value("${authMethod}")
    private String authMethod;
	
	@Value("${authParamId}")
    private String authParamId;
	
	@Value("${authParamOrg}")
    private String authParamOrg;
	
	@Value("${emailTo}")
	private String emailTo;
	
	@Value("${emailFrom}")
	private String emailFrom;
	
	@Value("${scheduleStart}")
	private String scheduleStart;
	
	@Value("${schedulerPeriod}")
	private String schedulerPeriod;
	
	@Value("${lastUpdated}")
	private String lastUpdated;
	
	@Value("${webUrl}")
    private String webUrl;
	
	@Value("${webSvcKey}")
    private String webSvcKey;
	
	@Value("${currentSemester}")
	private String currentSemester;
	
    @ManagedAttribute 
	public String getLearnUrl() {
        return learnUrl;
    }
    @ManagedAttribute 
    public void setLearnUrl(String learnUrl) {
        this.learnUrl = learnUrl;
    }
    @ManagedAttribute 
	public String getKey() {
        return key;
    }
   
    public void setKey(String key) {
        this.key = key;
    }
    
    @ManagedAttribute 
    public String getRestKey() {
        return restKey;
    }

    public void setRestKey(String key) {
        this.restKey = key;
    }
  
    @ManagedAttribute 
    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
    
    @ManagedAttribute 
    public String getRestSecret() {
        return restSecret;
    }

    public void setRestSecret(String secret) {
        this.restSecret = secret;
    }
    
    @ManagedAttribute
    public String getAuthEndpoint() {
    	return authEndpoint;
    }
   
    public void setAuthEndpoint(String endpoint) {
    	this.authEndpoint = endpoint;
    }
    @ManagedAttribute
    public String getAuthAction() {
    	return authAction;
    }
   
    public void setAuthAction(String action) {
    	this.authAction = action;
    }
    
    @ManagedAttribute
    public String getAuthMethod() {
    	return authMethod;
    }
   
    public void setAuthMethod(String method) {
    	this.authMethod = method;
    }
    
    @ManagedAttribute
    public String getAuthParamId() {
    	return authParamId;
    }
   
    public void setAuthParamId(String id) {
    	this.authParamId = id;
    }
    @ManagedAttribute
    public String getAuthParamOrg() {
    	return authParamOrg;
    }
   
    public void setAuthParamOrg(String org) {
    	this.authParamOrg = org;
    }
    
    @ManagedAttribute 
	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}
	
	@ManagedAttribute 
	public String getEmailFrom() {
		return emailFrom;
	}

	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}
	@ManagedAttribute 
	public String getScheduleStart() {
		return scheduleStart;
	}
	
	public void setScheduleStart(String f){
		this.scheduleStart = f;
	}
	
	@ManagedAttribute 
	public String getSchedulerPeriod() {
		return schedulerPeriod;
	}

	public void setSchedulerPeriod(String p) {
		this.schedulerPeriod = p;
	}
	
	@ManagedAttribute 
	public String getLastUpdated() {
		return this.lastUpdated;
	}
	public void setLastUpdated(String p) {
		this.lastUpdated = p;	
	}
    
	@ManagedAttribute 
	public String getWebUrl() {
		return this.webUrl;
	}
	public void setWebUrl(String webUrl) {
		this.webUrl = webUrl;
	}
	
	@ManagedAttribute 
	public String getWebSvcKey() {
		return this.webSvcKey;
	}
	public void setWebSvcKey(String key) {
		this.webSvcKey = key;
	}
	
	@ManagedAttribute 
	public String getCurrentSemester() {
		return this.currentSemester;
	}
	public void setCurrentSemester(String s) {
		this.currentSemester = s;
	}
}
	
