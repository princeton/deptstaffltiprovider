package edu.princeton.deptstafflti.entity;

public enum Role {
    
    INSTRUCTIONAL_STAFF("INSTRUCTIONAL_STAFF"), 
    COURSE_BUILDER("COURSE_BUILDER");
    
    
    public static final Role[] All = { INSTRUCTIONAL_STAFF, COURSE_BUILDER };
   
    private final String name;
    
    private Role(final String name) {
        this.name = name;
    }
    
    public static Role forName(final String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name cannot be null for Role");
        }
        if (name.toUpperCase().equals("INSTRUCTIONAL_STAFF")) {
            return INSTRUCTIONAL_STAFF;
        } else if (name.toUpperCase().equals("COURSE_BUILDER")) {
            return COURSE_BUILDER;
        }
        throw new IllegalArgumentException("Name \"" + name + "\" does not correspond to any Role");
    }
    public String getName() {
        return this.name;
    }
    
    @Override
    public String toString() {
        return getName();
    }  
}