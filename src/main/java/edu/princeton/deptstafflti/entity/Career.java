package edu.princeton.deptstafflti.entity;

public enum Career {
    
    ALL("ALL"), 
    UNDERGRADUATE("UNDERGRADUATE"),
    GRADUATE("GRADUATE");
    
    
    public static final Career[] All = { ALL, UNDERGRADUATE,GRADUATE  };
    private final String name;
    private Career(final String name) {
        this.name = name;
    }
    
    public static Career forName(final String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name cannot be null for type");
        }
        if (name.toUpperCase().equals("ALL")) {
            return ALL;
        } else if (name.toUpperCase().equals("UNDERGRADUATE")) {
            return UNDERGRADUATE;
        }else if (name.toUpperCase().equals("GRADUATE")) {
        return GRADUATE;
    }
        throw new IllegalArgumentException("Name \"" + name + "\" does not correspond to any Role");
    }
    public String getName() {
        return this.name;
    }
    
    @Override
    public String toString() {
        return getName();
    }
    
    
}