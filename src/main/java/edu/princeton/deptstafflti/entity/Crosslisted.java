package edu.princeton.deptstafflti.entity;

public enum Crosslisted {
    
	YES("YES"), 
    NO("NO");
   
    public static final Crosslisted[] All = {YES, NO  };
    private final String name;
    private Crosslisted(final String name) {
        this.name = name;
    }
    
    public static Crosslisted forName(final String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name cannot be null for type");
        }
        if (name.toUpperCase().equals("YES")) {
            return YES;
        } else if (name.toUpperCase().equals("NO")) {
            return NO;    
    }
        throw new IllegalArgumentException("Name \"" + name + "\" does not correspond to any Role");
    }
    public String getName() {
        return this.name;
    }
    
    @Override
    public String toString() {
        return getName();
    }
    
    
}