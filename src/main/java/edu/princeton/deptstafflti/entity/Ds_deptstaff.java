package edu.princeton.deptstafflti.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Id;

import java.sql.Timestamp;

@Entity
@Table (name="DSTAFF_DEPTSTAFF", schema="SISOUT")
public class Ds_deptstaff {

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DEPTSTAFF_LTI_SEQ")
    @SequenceGenerator(sequenceName = "DEPTSTAFF_LTI_SEQ", allocationSize = 1, name = "DEPTSTAFF_LTI_SEQ", schema="SISOUT")
	private Long id;
	private String dept;
	private String term;
	private String netid;
	private String emplid;
	private String career;
	private String role;
	private Timestamp created;
	private Timestamp disabled;
	private String crosslisted;
	
	public Ds_deptstaff() {}
	
	@Override
    public String toString() {
        return String.format(
                "DeptStaff_deptstaff[id=%d, dept=%s, term=%s, netid=%s, emplid='%s', career='%s', role='%s', created='%s']",
                id, dept, term, netid, emplid, career, role, created);
    }

	public String getDept() {
		return dept;
	}
	public String getTerm() {
		return term;
	}
	public String getEmplid() {
		return emplid;
	}
	
	public String getNetid() {
		return netid;
	}
	public String getRole() {
		return role;
	}
	public String getCareer() {
		return career;
	}
	public String getDisabled() {
		return disabled.toString();
	}
	public String getCrosslisted() {
		return crosslisted;
	}
	public void setEmplid(String emplid) {
		this.emplid = emplid;
	}
	public void setNetid(String trim) {
		netid = trim;	
	}
	public void setDept(String dept2) {
		dept = dept2;	
	}

	public void setTerm(String term2) {
		term = term2;
	}
	public void setCareer(String career2) {
		career = career2;
	}
	public void setRole(String role2) {
		role = role2;
		}
	public void setCrosslisted(String crosslisted2) {
		crosslisted = crosslisted2;	
	}
	public void setDisabled(Timestamp ts){
		disabled = ts;
	}

	public void setCreated(Timestamp timestamp) {
		created = timestamp;
		
	}
}