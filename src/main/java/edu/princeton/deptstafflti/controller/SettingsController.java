package edu.princeton.deptstafflti.controller;

import edu.princeton.deptstafflti.config.DeptStaffProperties;
import edu.princeton.deptstafflti.exception.GenericLtiException;
import edu.princeton.deptstafflti.model.SettingsModel;
import edu.princeton.deptstafflti.scheduler.ConfigureQuartz;
import edu.princeton.deptstafflti.scheduler.JobWithSimpleTrigger;
import edu.princeton.deptstafflti.service.SettingsService;

import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.quartz.TriggerKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;

@Controller
public class SettingsController {

    @Autowired
    SettingsService settingsService;
    
	@Autowired 
	private DeptStaffProperties properties;
	
	@Autowired
	 private org.springframework.cloud.endpoint.RefreshEndpoint endPoint;
	
	@Autowired
	private SchedulerFactoryBean schedFactory;
	    
    private static final Logger logger = LoggerFactory
			.getLogger(SettingsController.class);

    // presents the setting page form
   
    @RequestMapping(value = {"/settings"}, method = {RequestMethod.GET})
    public String loadSettings(HttpServletRequest request, Model model) {

        SettingsModel settingsModel = settingsService.loadSettings();

        model.addAttribute("settings", settingsModel);
        model.addAttribute("noChangeMsg", false);
		model.addAttribute("errMsg", false);
        model.addAttribute("lastUpdated", settingsModel.getLastUpdated());
        return "settings";
    }

    @RequestMapping(value = {"/settings"}, method = {RequestMethod.POST}, params = {"save"})
    public String saveSettings (@ModelAttribute("settings")SettingsModel settingsModel, Model model) {
    	SettingsModel origsettings = settingsService.loadSettings();
    	boolean updated = true;
    	updated = settingsModel.equals(origsettings);
    	if (updated == true){
    		try {
    			settingsService.saveSettings(settingsModel);
    			endPoint.invoke();
    			return "forward:/unrescheduleTrigger";
    		} catch (GenericLtiException ex) {
    			logger.debug("Error occurred on saving the new settings" + ex.toString());
    			model.addAttribute("errMsg", "true");
       		}
        }else {
        	model.addAttribute("noChangeMsg", true);
        	model.addAttribute("errMsg", false);
        }
    	return "settings";
    }
    
    @RequestMapping(value ={"/unrescheduleTrigger"}, method = {RequestMethod.POST})
    public String reschedule (HttpServletRequest request, Model model) {
    	//destroy the current 
    	TriggerKey tkey = new TriggerKey("simpleTrigger");
		JobKey jkey = new JobKey("jobBean"); 
		SettingsModel settingsModel = settingsService.loadSettings();
		long p  = 3000000L;
		try {
			schedFactory.getScheduler().unscheduleJob(tkey);
			schedFactory.getScheduler().deleteJob(jkey);
		} catch (SchedulerException e) {
			logger.debug("Error while unscheduling " + e.getMessage());
		}
		try {
			JobDetailFactoryBean jdfb = ConfigureQuartz.createJobDetail(JobWithSimpleTrigger.class);
			jdfb.setBeanName("jobBean");
			jdfb.afterPropertiesSet();
			//P is in second, convert to microsecond
			long frequency = Long.parseLong(settingsModel.getSchedulerPeriod()) == 0 ? p:Long.parseLong(settingsModel.getSchedulerPeriod()) * 1000;
	    	SimpleTriggerFactoryBean stfb = ConfigureQuartz.createTrigger(jdfb.getObject(),frequency);
			stfb.setBeanName("simpleTrigger");
			stfb.afterPropertiesSet();
			schedFactory.getScheduler().scheduleJob(jdfb.getObject(), stfb.getObject());
		}catch(Exception e){
			logger.debug("Error while scheduling " + e.getMessage());
		}
        model.addAttribute("settings", settingsModel);
        model.addAttribute("noChangeMsg", false);
		model.addAttribute("errMsg", false);
        model.addAttribute("lastUpdated", settingsModel.getLastUpdated());
        return "settings";
	
    }
    
}
