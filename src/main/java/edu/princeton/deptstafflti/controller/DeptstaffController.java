package edu.princeton.deptstafflti.controller;

import edu.princeton.deptstafflti.config.DeptStaffProperties;
import edu.princeton.deptstafflti.data.Subject;
import edu.princeton.deptstafflti.data.Term;
import edu.princeton.deptstafflti.entity.Career;
import edu.princeton.deptstafflti.entity.Crosslisted;
import edu.princeton.deptstafflti.entity.Role;
import edu.princeton.deptstafflti.model.DeptSModel;
import edu.princeton.deptstafflti.model.DeptStaffEnrollModel;
import edu.princeton.deptstafflti.model.SettingsModel;
import edu.princeton.deptstafflti.service.DataFetcherService;
import edu.princeton.deptstafflti.service.DeptStaff;
import edu.princeton.deptstafflti.service.SettingsService;
import edu.princeton.deptstafflti.service.JsonDAO;
import edu.princeton.deptstafflti.service.Util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Controller
public class DeptstaffController {
	
	private static final Logger logger = LoggerFactory
			.getLogger(LaunchController.class);
	
	@Autowired
    DataFetcherService dataFetcherService;
	@Autowired
	DeptStaff deptstaff;
	@Autowired
	JsonDAO jsonDao;
	@Autowired
	DeptStaffProperties properties;
	@Autowired
	SettingsService settingsService;
	 
    @ModelAttribute("terms")
    public List<Term> populateTerms() {
        List<Term> terms = Util.buildTermList();
        return terms;
    }
    @ModelAttribute("allRoles")
    public List<Role> populateRoles() {
        return Arrays.asList(Role.All);
    }
    @ModelAttribute("allCareers")
    public List<Career> populateCareers() {
        return Arrays.asList(Career.All);
    }
    @ModelAttribute("allCrosslisted")
    public List<Crosslisted> populateCrosslisted() {
        return Arrays.asList(Crosslisted.All);
    }
   
	@RequestMapping(value = {"/","/retrive"}, method = {RequestMethod.GET, RequestMethod.POST})
	public String showDeptStaff(HttpServletRequest request, Model model) {
		
		DeptSModel dsModel = new DeptSModel();
		SettingsModel settingsModel = settingsService.loadSettings();
		//get current term,i.e. currentSemester: F2017; currentTerm:Fall2017
		String currentSemester = settingsModel.getCurrentSemester();
		String userName = request.getParameter("user_id");
		dsModel.setCaller(userName);
		dsModel.setIsAdmin("false");
		String[] roles = request.getParameter("roles").split(",");
		boolean isAdmin = false;
		if(roles.length > 0) {
			for(String role : roles){
				logger.debug("role: " + role);
				if("Administrator".equals(role.trim())) {
					isAdmin = true;
					dsModel.setIsAdmin("true");
					break;
				}
			}
		}
		List<Subject> subjects = getSubjectListByUser(userName, currentSemester, isAdmin);
		model.addAttribute("subjects", subjects);
		
		Term t = new Term();
		t.setId(currentSemester);
		t.setName(Util.sem2term(currentSemester));
		dsModel.setCurrentTerm(t);
		List<DeptStaffEnrollModel> adminList = new ArrayList<DeptStaffEnrollModel>();
		adminList.clear();
		model.addAttribute("deptStaffEnrollModelList", adminList);
		Subject s = new Subject();
		s.setId("--Pick a subject--");
		s.setName("--Pick a subject--");
		dsModel.setCurrentSubject(s);
		model.addAttribute("dsModel", dsModel);
		return "deptstaff";
	}
	
	@RequestMapping(value = {"/deptstaff"}, method = {RequestMethod.GET, RequestMethod.POST})
	public String getDeptStaff(HttpServletRequest request, DeptSModel dsModel,  Model model){
		
		String subject = dsModel.getCurrentSubject().getId();
		String term = dsModel.getCurrentTerm().getId();
		String isAdmin = dsModel.getIsAdmin();
		String caller = dsModel.getCaller();
		boolean admin = false;
		if(("true").equals(isAdmin)){
			admin = true;
		}
		List<Subject> subjects = getSubjectListByUser(caller, term, admin);
		model.addAttribute("subjects", subjects);
		List<DeptStaffEnrollModel> adminList = getAdminList(term.trim(), subject.trim());
		model.addAttribute("spansubject", subject);
		
		model.addAttribute("spanterm", term);
		model.addAttribute("deptStaffEnrollModelList", adminList);
		model.addAttribute("dsModel", dsModel);
		return "deptstaff";
		
	}
	
	@RequestMapping(value={"/deptstaff"}, params = {"save"}, method = {RequestMethod.POST})
	public String saveCourseAdmin(@Valid DeptSModel dsModel, BindingResult bindingResult, final HttpServletRequest req, Model model) {
		logger.debug("call add" );
		String subject = dsModel.getCurrentSubject().getId();
		String term = dsModel.getCurrentTerm().getId();
		String netids = dsModel.getNetids();
		String career = dsModel.getCareer().getName();
		String isAdmin = dsModel.getIsAdmin();
		String caller = dsModel.getCaller();
		String roleid = Util.getRoleId(dsModel.getRole().getName());
		String crosslisted = dsModel.getCrosslisted().getName();
		String invalidIds = null;
		boolean admin = false;
		if(("true").equals(isAdmin)){
			admin = true;
		}
		
		if(bindingResult.hasErrors()){
			List<Subject> subjects = getSubjectListByUser(caller, term, admin);
			model.addAttribute("subjects", subjects);
			List<DeptStaffEnrollModel> adminList = getAdminList(term.trim(), subject.trim());
			model.addAttribute("deptStaffEnrollModelList", adminList);
			model.addAttribute("spansubject", subject);
			model.addAttribute("spanterm", term);
			model.addAttribute("dsModel",dsModel);
			return "deptstaff";
		}
		try {
			invalidIds = deptstaff.addUserToDeptLists(subject, term, netids, career, roleid, crosslisted);
		}catch (Exception ex) {
			logger.error("Error save enrollment, " + ex.toString());
		}
		List<Subject> subjects = getSubjectListByUser(caller, term, admin);
		model.addAttribute("subjects", subjects);
		List<DeptStaffEnrollModel> adminList = getAdminList(term.trim(), subject.trim());
		model.addAttribute("deptStaffEnrollModelList", adminList);
		model.addAttribute("spansubject", subject);
		model.addAttribute("spanterm", term);
		//clear the input textbox
		dsModel.setNetids("");
		model.addAttribute("dsModel",dsModel);
		if(invalidIds != null && invalidIds.length() > 0){
			model.addAttribute("invalidIds", invalidIds);
		}
		return "deptstaff";
	}
	
	@RequestMapping(value={"/deptstaff"}, params = "delete", method = {RequestMethod.POST})
	public String removeCourseAdmin(DeptSModel dsModel, final HttpServletRequest req, Model model, @RequestParam(value="delete", required = true) String action) {
		logger.debug("*********called delete");
		String subject = dsModel.getCurrentSubject().getId();
		String term = dsModel.getCurrentTerm().getId();
		String isAdmin = dsModel.getIsAdmin();
		String caller = dsModel.getCaller();
		boolean admin = false;
		if(("true").equals(isAdmin)){
			admin = true;
		}
		List<Subject> subjects = getSubjectListByUser(caller, term, admin);
		model.addAttribute("subjects", subjects);
		String netid = req.getParameter("delete");
		logger.debug(" term: " + term + " ,subject: " + subject + " ,netid: " + netid);
		try {
			deptstaff.deleteUserFromDeptLists(term, subject, netid);
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("Error delete enrollment, " + ex.toString());
		}
		List<DeptStaffEnrollModel> adminList = getAdminList(term.trim(), subject.trim());
		model.addAttribute("deptStaffEnrollModelList", adminList);
		model.addAttribute("spansubject", subject);
		model.addAttribute("spanterm", term);
		model.addAttribute("dsModel", dsModel);
		return "deptstaff";
	}
	
	public List<DeptStaffEnrollModel> getAdminList(String term, String subject){
			logger.debug("term: " + term.trim() + ", subject: " + subject.trim());
		List<DeptStaffEnrollModel> adminList = deptstaff.getCourseAdmins(term.trim(), subject.trim());
		return adminList;
	}
	public List<Subject> getSubjectListByUser(String userName,String term, boolean isAdmin){
		List<Subject> subjects = new ArrayList<Subject>();
		
		Set<String>depts = null;
		
		if(isAdmin == true) {
				logger.debug("will call find All Subjects");
				depts = deptstaff.findAllSubjects(term);
		}else {
			try {
				depts = deptstaff.getSubjects(userName,term);
			} catch (Exception ex) {
				logger.debug("Error retriving data from getSubjects." + ex.toString());
				return null;
				}
			}
		if (depts != null) {
			depts.forEach(dept -> {
				Subject s = new Subject();
				s.setId(dept);
				s.setName(dept);
				subjects.add(s);
			});
		}
		return subjects ;
	}	
}
		