package edu.princeton.deptstafflti.controller;

import edu.princeton.deptstafflti.exception.GenericLtiException;
import edu.princeton.deptstafflti.model.SettingsModel;
import edu.princeton.deptstafflti.service.SettingsService;
import edu.princeton.deptstafflti.service.DataFetcherService;
import edu.princeton.deptstafflti.Constants;
import net.oauth.*;
import net.oauth.server.HttpRequestMessage;
import net.oauth.server.OAuthServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.net.URLDecoder;
import java.util.Iterator;
import java.util.Map;

@Controller
public class LaunchController {
	
	private static final Logger logger = LoggerFactory
			.getLogger(LaunchController.class);
	
	@Autowired
    DataFetcherService dataFetcherService;
	
	@Autowired
	SettingsService settingsService;
	
	@RequestMapping("/hello")
	public String hello(HttpServletRequest request, HttpServletResponse response, Model model){
		return "hello";		
	}
	
	@RequestMapping(value = {"/launch"}, method = {RequestMethod.GET, RequestMethod.POST})
	public String launch(HttpServletRequest request, HttpServletResponse response, Model model) {
		
		 String parmsForDisplay = "";
	     Map parameters = request.getParameterMap();
	     for (Iterator i = parameters.keySet().iterator(); i.hasNext();) {
	    	 String key = (String) i.next();
	         parmsForDisplay += key +": ";
	         String[] values = (String[]) (parameters.get(key));
	         for (int j = 0; j < values.length; j++) {
	        	 if (j != 0) {
	        		 parmsForDisplay += ",&nbsp";
	                }
	                parmsForDisplay += values[j];
	                parmsForDisplay += "&nbsp&nbsp";
	            }  
	        }
	        model.addAttribute("ltiParms", parmsForDisplay);
	       /*
	        SettingsModel settingsModel = null;
	        try {
	            settingsModel = settingsService.loadSettings();
	            if (authenticate(request, settingsModel)) {
	            	logger.debug("authenticated");
	               // String crs_uuid = request.getParameter("context_id");
	               // model.addAttribute("courseId", crs_uuid);

	                String user_uuid = request.getParameter("user_id");
	                model.addAttribute("userId", user_uuid);
	                String[] roles = request.getParameterValues("roles");
	                for(String role : roles){
	                	logger.debug("roles: " + role);
	                	if(isAuthorized(role) == true){
	                		logger.debug("will forward to DeptstaffController");
	                		return "forward:/deptstaff";
	                	}
	                }
	                return "forward:/error/view";
	            }
	        } catch (Exception e) {
	        	logger.debug("Exception thrown when launch, " + e.toString());
	            return "launcherror";
	        } 
	        */
	        return "forward:/retrive";
	       
	    
	}

	private boolean isAuthorized(String roles) {
		
		String[] each = roles.split(",");
		if(each != null){
			for(String role:each) {
				logger.debug("role: " + role);
				if(Constants.INSTRUCTOR_ROLE.equals(role) || Constants.ADMINISTRATOR_ROLE.equals(role)){
					return true;
				}
			}
		}
		return false;
	}

	public boolean authenticate(HttpServletRequest request, SettingsModel settingsModel) throws GenericLtiException {

        OAuthValidator validator = new SimpleOAuthValidator();
        String oauth_consumer_key = null;

        try {
            OAuthMessage oauthMessage = OAuthServlet.getMessage(request, null);
            oauth_consumer_key = oauthMessage.getConsumerKey();

            // callback URL syntax per LTI spec
            OAuthConsumer consumer = new OAuthConsumer(
                    "about:blank",
                    oauth_consumer_key,
                    settingsModel.getSecret(),
                    null);

            String signatureMethod = oauthMessage.getSignatureMethod();
            String signature = URLDecoder.decode(oauthMessage.getSignature(), "UTF-8");

            // all tokens are empty
            OAuthAccessor accessor = new OAuthAccessor(consumer);
            validator.validateMessage(oauthMessage, accessor);
        } catch (Exception e) {
        	logger.debug(e.getMessage());
            throw new GenericLtiException(e);
        }

        return true;
    }

    // Everything below was constructed  based on the OAuth code example below:
    // http://oauth.googlecode.com/svn/code/java/core/
    //
    /**
     * Extract the parts of the given request that are relevant to OAuth.
     * Parameters include OAuth Authorization headers and the usual request
     * parameters in the query string and/or form encoded body. The header
     * parameters come first, followed by the rest in the order they came from
     * request.getParameterMap().
     *
     * @param URL the official URL of this service; that is the URL a legitimate
     *            client would use to compute the digital signature. If this parameter is
     *            null, this method will try to reconstruct the URL from the HTTP request;
     *            which may be wrong in some cases.
     */
	
    public static OAuthMessage getMessage(HttpServletRequest request, String URL) {
        if (URL == null) {
            URL = request.getRequestURL().toString();
        }
        int q = URL.indexOf('?');
        if (q >= 0) {
            URL = URL.substring(0, q);
            // The query string parameters will be included in
            // the result from getParameters(request).
        }
        return new HttpRequestMessage(request, URL);
    }

    /**
     * Reconstruct the requested URL, complete with query string (if any).
     */
	
    public static String getRequestURL(HttpServletRequest request) {
        StringBuffer url = request.getRequestURL();
        String queryString = request.getQueryString();
        if (queryString != null) {
            url.append("?").append(queryString);
        }
        return url.toString();
    }
}
