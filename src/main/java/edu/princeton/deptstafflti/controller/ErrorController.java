package edu.princeton.deptstafflti.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ErrorController {

    @RequestMapping(value = {"/error/view"})
    public String unauthorizedView(@RequestParam(value="name", required=false, defaultValue="kimhuang") String name, Model model) {
        model.addAttribute("name", name);
        model.addAttribute("errorcode", "unauthorizedView");
        return "unauthorized";
    }
    @RequestMapping(value = {"/error/edit"})
    public String UnauthorizedEdit(@RequestParam(value="name", required=false, defaultValue="kimhuang") String name, Model model) {
        model.addAttribute("name", name);
        model.addAttribute("errorcode", "unauthorizedEdit");
        return "unauthorized";
    }
}