package edu.princeton.deptstafflti;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.princeton.deptstafflti.config.DeptStaffProperties;


@SpringBootApplication
@EnableCaching
public class Application extends WebMvcConfigurerAdapter {
	private static final Logger log = LoggerFactory.getLogger(Application.class);
	
	@Autowired
	DeptStaffProperties properties;
	
	
    public static void main(String[] args) {
    	final String ORACLE_TNS = "/Users/kimhuang/.sqldeveloper/network/admin";
    	System.setProperty("oracle.net.tns_admin", ORACLE_TNS);
        SpringApplication.run(Application.class, args);
    }
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/assets/**").addResourceLocations("classpath:/assets/");
    }
    /*
    @Bean
    public CommandLineRunner demo (PhotoRepository repos){
    	return (args) -> {
    		repos.save(new Sitephoto_emplid("091612345","Yes"));
    		repos.save(new Sitephoto_emplid("091783456","Yes"));
    		log.info("-------------------------");
    		for(Sitephoto_emplid emplid:repos.findAll()){
    			log.info(emplid.toString());
    		}
    	};
    }
    */
}