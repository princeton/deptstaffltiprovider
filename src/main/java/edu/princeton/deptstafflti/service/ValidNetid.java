package edu.princeton.deptstafflti.service;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;
import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ValidNetidValidator.class)
public @interface ValidNetid {
    String message() default "{edu.princeton.deptstafflti.invalidnetid.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}