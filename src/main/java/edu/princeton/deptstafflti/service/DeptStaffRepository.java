package edu.princeton.deptstafflti.service;

import java.util.List;

import edu.princeton.deptstafflti.data.CourseAdmin;
import edu.princeton.deptstafflti.entity.Ds_deptstaff;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DeptStaffRepository extends CrudRepository<Ds_deptstaff, Long> {
		 
		/*
		  select dept, term, netid, emplid, career, role, crosslisted from DSTAFF_DEPTSTAFF
		  	where term >= :term and disabled is null order by 2,1;
		 */
		@Query("select s.dept, s.term, s.netid, s.emplid, s.career, s.role, s.crosslisted from "
				+ "Ds_deptstaff s where s.term >= :term and s.disabled is null order by s.term, s.dept ")
		List<CourseAdmin> getCourseAdmins(@Param("term")String term);
		
		@Query("select s from Ds_deptstaff s "
				+ "where s.dept= :dept and s.term= :term and s.disabled is null order by s.career desc, s.netid, s.created")
		List<Ds_deptstaff> lookupAdmins(@Param("term")String term, @Param("dept")String dept);
		
		@Query("select s from Ds_deptstaff s "
				+ "where s.dept= :dept and s.term= :term and s.netid= :netid and s.disabled is null")
		Ds_deptstaff getOneCourseAdmin(@Param("term")String term, @Param("dept")String dept, @Param("netid")String netid);
		
		@Query("select s from Ds_deptstaff s "
				+ "where s.dept= :dept and s.term= :term and s.netid= :netid "
				+ "and s.role= :role and s.career= :career and s.crosslisted = :crosslisted")
		Ds_deptstaff findSameCourseAdmin(@Param("term")String term, 
				@Param("dept")String dept, @Param("netid")String netid, @Param("role")String role, @Param("career")String career, @Param("crosslisted")String cross);	

}