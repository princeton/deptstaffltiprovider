package edu.princeton.deptstafflti.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import edu.princeton.deptstafflti.data.Term;
//import org.apache.log4j.Logger;

public class Util {
	//private static Logger log = Logger.getLogger(Util.class);
	
	/**
	 * @param s
	 * @return a list extracted from String array 
	 */
	public static List<String> stringArrayToList(String[] arr) {
		List<String> list = new ArrayList<String>();
		if (arr == null || arr.length == 0) {
		} else {
			for (String s: arr) {
				list.add(s);
			}
		}
		return list;
	}
	
	/**
	 * Convert a semester to an strm, for example:
	 * 		S2010 -> 1104
	 * 		F2010 -> 1112
	 * 		S2011 -> 1114
	 * 		F2011 -> 1122
	 * 		S2012 -> 1124
	 * 
	 * @param semester
	 * @return an strm value for the semester, for example F2010 -> 1112
	 * @throws Exception
	 */
	public static String term2Strm(String semester) throws Exception {
		String strm = "";

		if (semester == null) {
			throw new Exception("semester is null");
		} else {
			int semesterLen = semester.length();
			
			if (semesterLen == 5 || semesterLen == 6) {
				String semesterIdentifier	= (semesterLen == 5)
						? semester.substring(0, 1)
						: semester.substring(0, 2);
				String semesterYear 		= (semesterLen == 5)
						? semester.substring(1)
						: semester.substring(2);

				//log.debug("semesterIdentifier=" + semesterIdentifier);
				//log.debug("semesterYear=" + semesterYear);
						
				// compute strmValue as int
				String strmSemesterIdentifier 	= "";
				int strmSemesterYear 			= Integer.parseInt(semesterYear) - 1900;

				if (semesterIdentifier.equals("SU")) {
					strmSemesterIdentifier = "1";
					strmSemesterYear++; // set to 'course year' for the summer semester
				} else if (semesterIdentifier.equals("F")) {
					strmSemesterIdentifier = "2";
					strmSemesterYear++; // set to 'course year' for the fall semester
				} else if (semesterIdentifier.equals("S")) {
					strmSemesterIdentifier = "4";
				} else {
					throw new Exception(semester + " has unrecognized semester identifier=" + semesterIdentifier);
				}
				   
				strm = "" + strmSemesterYear + strmSemesterIdentifier; 
			} else {
				throw new Exception("semester is invalid length: semester=" + semester);
			}
		}
		
		if (strm.equals("")) {
			throw new Exception("unable to calculate strm value for semester=" + semester);
		}

		return strm;
	}
	
	/**
	 * The default checkString method.
	 * 
	 * @param s
	 * @return an empty String if s is null, otherwise s.trim()
	 */
	public static String checkString(String s) {
		return checkString(s, true);
	}
	
	/**
	 * @param s
	 * @param trim
	 * @return empty String if s is null, otherwise s.trim() if 'trim' is true
	 *         or s if 'trim' is false
	 */
	public static String checkString(String s, boolean trim) {
		return (s == null) ? "" : (trim) ? s.trim() : s;
	}

	/**
	 * @param s
	 * @param defaultValue
	 * @return defaultValue if s is null or s.trim() is empty, otherwise s.trim()
	 */
	public static String checkString(String s, String defaultValue) {
	    s = checkString(s, true);
	    return (s.equals("")) ? defaultValue : s;
	}
	
	/**
	 * @param className
	 * @return the unqualified className (package info removed)
	 */
	public static String getUnqualifiedClassName(String className) {
		int pos = className.lastIndexOf(".");
	    return (pos == -1) ? className : className.substring(pos + 1);
	}
	
	public static List<Term> buildTermList() {
		LocalDate today = LocalDate.now();
		int month = today.getMonthValue();
		int year = today.getYear();
		List<Term> ret = new ArrayList<Term>();
		Term t = new Term();
		String fs = "F";
		if (month < 6) { 
			fs="S";
		}else if (month < 7) {
			fs="SU";
		}
		if (fs == "F") {
			year += 1;
		}
		for (int i = year; i>= 2015; i--) {
			t = new Term();
			
			t.setId("F" + i);
			t.setName("Fall" + i);
			ret.add(t);
			t = new Term();
			t.setId("SU" + i);
			t.setName("Summer" + i);
			ret.add(t);
			t = new Term();
			t.setId("S" + i);
			t.setName("Spring" + i);
			ret.add(t);
		}
		return ret;
	}

	public static String sem2term(String s) {
		if (s == null) {
			return null;
		}
		String year = s.substring(s.length()-4);
		String term = s.substring(0, s.length()-4);

		if ( "SU".equals(term) ) {
			return "Summer" + year;
		} 
		if ( "F".equals(term) ) {
			return "Fall" + year;
		} 
		if ( "S".equals(term) ) {
			return "Fall" + year;	
		}
		return null;
	}
	public static String term2sem(String s) {
		if (s == null) {
			return null;
		}
		String year = s.substring(s.length()-4);
		String term = s.substring(0, s.length()-4);

		if ( "SUMMER".equals(term.toUpperCase()) ) {
			return "SU" + year;
		} 
		if ( "FALL".equals(term.toUpperCase()) ) {
			return "F" + year;
		} 
		if ( "SPRING".equals(term.toUpperCase()) ) {
			return "S" + year;	
		}
		return null;
	}

	public static String getRoleId(String name) {
		if (name == null) {
			return null;
		}
		switch (name) {
			case "INSTRUCTIONAL_STAFF" :
				return "TeachingAssistant";
			case "COURSE_BUILDER" :
				return "CourseBuilder";
		}
		return "CourseBuilder";
			
	}
}
