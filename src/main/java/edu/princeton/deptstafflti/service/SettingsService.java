package edu.princeton.deptstafflti.service;


import org.springframework.stereotype.Service;

import edu.princeton.deptstafflti.model.SettingsModel;
import edu.princeton.deptstafflti.exception.GenericLtiException;

@Service
public interface SettingsService {
	
    public SettingsModel loadSettings ();
    public void saveSettings (SettingsModel settingsModel) throws GenericLtiException;
}