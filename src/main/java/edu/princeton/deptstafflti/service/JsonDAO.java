package edu.princeton.deptstafflti.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Service;

import edu.princeton.deptstafflti.service.SettingsService;
import edu.princeton.deptstafflti.model.SettingsModel;

@Service
public class JsonDAO {
	private static Logger log = LoggerFactory.getLogger(JsonDAO.class);
	
	private final SettingsService settingsService;
	
	
	public JsonDAO(SettingsService service){
		this.settingsService = service;
	}
	
	public JSONArray findAllSubjects(String year, String term){
		JSONArray params = new JSONArray();
		params.add(year);
		params.add(term);
		return fetch("services.php/DeptStaff/findAllSubjects",  params);
	}

	public JSONArray getSubjects(Set<String> depts){
		JSONArray params = new JSONArray();
		for(String d: depts){
			params.add(d);
		}
		return fetch("services.php/DeptStaff/getSubjects",  params);
	}
	
	public JSONArray getCourseBatchUidsUngd(String dept, String strm, Boolean doCrossListed){
		JSONArray params = new JSONArray();
		params.add(dept);
		params.add(strm);
		if(doCrossListed) {
			params.add(strm);
			params.add(dept);
			params.add(strm);
			return fetch("services.php/DeptStaff/getCourseBatchUidsUngdX",  params);
		}
		return fetch("services.php/DeptStaff/getCourseBatchUidsUngd", params);
	}
	
	public JSONArray getCourseBatchUidsGrad(String dept, String strm, Boolean doCrossListed){
		JSONArray params = new JSONArray();
		params.add(dept);
		params.add(strm);
		if(doCrossListed) {
			params.add(strm);
			params.add(dept);
			params.add(strm);
			return fetch("services.php/DeptStaff/getCourseBatchUidsGradX",  params);
		}
		return fetch("services.php/DeptStaff/getCourseBatchUidsGrad", params);
	}
	
	public JSONArray getCourseBatchUids4All(String dept, String strm, Boolean doCrossListed){
		JSONArray params = new JSONArray();
		params.add(dept);
		params.add(strm);
		if(doCrossListed){
			params.add(strm);
			params.add(dept);
			params.add(strm);
			return fetch("services.php/DeptStaff/getCourseBatchUids4AllX",  params);
		}
		return fetch("services.php/DeptStaff/getCourseBatchUids4All",  params);
	}
	
	public String getCurrentStrm(){
		JSONArray params = new JSONArray();
		
		JSONArray data = fetch("services.php/DeptStaff/getCurrentStrm",  params);
		
		return (data.size() > 0) ? (String)((JSONObject)data.get(0)).get("STRM") : "";
	}
	private JSONArray fetch(String command, JSONArray params){
		StringBuffer response = new StringBuffer();
		SettingsModel settingsModel = null;
		try {
			settingsModel = settingsService.loadSettings();
		 }catch(Exception e) {
    		 log.error("exceptions: " + e.toString());
    		 return null;
    	 }
		log.debug("settingsModel.getWebUrl()" + settingsModel.getWebUrl());
		log.debug("settingsModel.getWebSvcKey()" + settingsModel.getWebSvcKey());
		try {	
			URL obj = new URL(settingsModel.getWebUrl() + command);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			
			con.setDoInput(true);
			con.setDoOutput(true);
			
			// Send post request
			con.setRequestMethod("POST");
			con.setRequestProperty("X-PU-B2AUTH", settingsModel.getWebSvcKey());
			
			if(params != null){
				DataOutputStream wr = new DataOutputStream(con.getOutputStream());
				wr.writeBytes(params.toString());
				wr.flush();
				wr.close();
			}
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			//log.debug(response.toString());
			JSONParser parser = new JSONParser();
			JSONArray a = (JSONArray)parser.parse(response.toString());
			return a;
		} catch (Exception ex) {
			// TODO Handle exceptions.
			log.error(ex.toString());
			ex.printStackTrace();
		}
		return null;
	}
}
