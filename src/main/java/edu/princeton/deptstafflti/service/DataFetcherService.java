package edu.princeton.deptstafflti.service;

import java.util.List;

import org.springframework.stereotype.Service;

import edu.princeton.deptstafflti.data.DataSource;
import edu.princeton.deptstafflti.data.Token;
import edu.princeton.deptstafflti.data.User;
import edu.princeton.deptstafflti.model.SettingsModel;
import edu.princeton.deptstafflti.exception.GenericRestException;

@Service
public interface DataFetcherService {
	public Token authorize (SettingsModel settingsModel) throws GenericRestException;

	public String deleteCourseMembership(SettingsModel settings, Token token, String uuid, String courseBatchUid ) throws Exception;
	public User getUser(SettingsModel settings, Token token, String netid) throws Exception;
	public List<User> getUsers(SettingsModel settings, Token token, String[] netIds) throws Exception;
	public DataSource getDataSource(SettingsModel settings, Token token, String batchuid, String description) throws Exception;
	public String addDataSource(SettingsModel settings, Token token, String batchuid, String description) throws Exception;
	public String createCourseMembership(SettingsModel settings, Token token, String userExternalId, String CourseExternalId, String datasource, String courseRole) throws Exception;
	public List<String> validateUsers(SettingsModel settings, Token token, String[] netIds);
	public String createCourseMembershipByPUId(SettingsModel settings, Token token, String uuid, String courseBatchUid, String dsBatchUid, String role) throws Exception;
}