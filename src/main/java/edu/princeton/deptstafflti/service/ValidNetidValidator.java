package edu.princeton.deptstafflti.service;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import edu.princeton.deptstafflti.data.Token;
import edu.princeton.deptstafflti.model.SettingsModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class ValidNetidValidator implements ConstraintValidator<ValidNetid, String> {
 
	@Autowired
	DataFetcherService dataFetcher;
	@Autowired
	SettingsService settingsService;
	
	private Logger log 							= LoggerFactory.getLogger(getClass());
    public ValidNetidValidator() {
      
    }
 
    public void initialize(ValidNetid constraint) {
    }
 
    public boolean isValid(String netids, ConstraintValidatorContext context) {
    	Token token = null;
    	SettingsModel settings = null;
    	try {
    		settings = settingsService.loadSettings();
    		token = dataFetcher.authorize(settings);
    	}catch(Exception ex){
    		log.error("Can't grab token, therefore can't valid the user netid." + ex.getMessage());
    		return false;
    	}
    	String[] netidArray = netids.split(",");
    	List<String> ids = dataFetcher.validateUsers(settings,  token,  netidArray);
    	log.debug("ids.size(): " + ids.size());
    	
        return (!(ids.size() > 0));
    }
 
}