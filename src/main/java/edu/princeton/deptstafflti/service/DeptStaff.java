package edu.princeton.deptstafflti.service;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.filter.ElementFilter;
import org.jdom.input.SAXBuilder;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

import edu.princeton.deptstafflti.Constants;
import edu.princeton.deptstafflti.data.CourseAdmin;
import edu.princeton.deptstafflti.data.CourseAdminRpt;
import edu.princeton.deptstafflti.data.DataSource;
import edu.princeton.deptstafflti.data.Token;
import edu.princeton.deptstafflti.data.User;
import edu.princeton.deptstafflti.exception.AuthorizationFailure;
import edu.princeton.deptstafflti.entity.Ds_deptstaff;
import edu.princeton.deptstafflti.model.DeptStaffEnrollModel;
import edu.princeton.deptstafflti.model.SettingsModel;
import edu.princeton.deptstafflti.service.JsonDAO;

@Service
public class DeptStaff {
	
	@Autowired 
	DataFetcherService dataFetcher;
	@Autowired 
	JsonDAO json;	
	@Autowired
	SettingsService settingsService;
	@Autowired
	DeptStaffRepository repository;
	
	private Logger log 							= LoggerFactory.getLogger(getClass());
	
	boolean isUpdateAllDeptEnrollmentsRunning	= false;

	public List<User> addEnrollments(
			String[] netIds,
			String dept,
			String semester,
			String career,
			String role,
			String crosslisted) throws Exception {
		
		log.debug("dept=" + dept);
		Token token = null;
		SettingsModel settings = settingsService.loadSettings();
		List<User> users = null;
		try {
			token = dataFetcher.authorize(settings);
		} catch (Exception ex) {
			log.debug("exception on authorization", ex);
		}
		
		// get the list of users
		try {
			users = dataFetcher.getUsers(settings, token, netIds);
		} catch (Exception ex) {
			log.debug("error getUsers", ex);
		}
		boolean doCrossListed = "Y".equals(crosslisted);
		
		// get the list of courseBatchUids
		List<String> courseBatchUids = getCourseBatchUids(
				dept, 
				semester, 
				career, 
				doCrossListed);

		// get the dataSource
		// note: default is '2	0	SYSTEM	System data source used for associating 
		//       records that are created via web browser.' 
		DataSource dataSource = dataFetcher.getDataSource(settings, token, Constants.DS_BATCH_UID, Constants.DS_DESCRIPTION);
		for (String courseBatchUid: courseBatchUids) {
			log.debug("courseBatchUid: " + courseBatchUid);

			for (User user: users) {	
				try {
					String ret = dataFetcher.createCourseMembership(settings, token, user.getUuid(), courseBatchUid, Constants.DS_BATCH_UID, role );
				}catch(Exception ex) {
					log.debug("exception on adding course enrollment" + ex.toString());
				}
			}
		}
		log.debug("done");
		return users;
	}
	
	public List<CourseAdminRpt> updateAllDeptEnrollments() throws Exception {
		
		List<CourseAdminRpt> courseAdminResults = new ArrayList<CourseAdminRpt>();
		List<CourseAdmin> courseAdmins = null;
		Token token = null;
		SettingsModel settings = settingsService.loadSettings();
		try {
			dataFetcher.authorize(settings);
		} catch (Exception ex) {
			log.debug("exception on authorization", ex);
		}
		try {
			String currentStrm;
			currentStrm = json.getCurrentStrm();
			log.debug("currentStrm retrieved from WS: currentStrm=" + currentStrm);

			courseAdmins = repository.getCourseAdmins(currentStrm);
			DataSource dataSource = dataFetcher.getDataSource(settings, token, Constants.DS_BATCH_UID, Constants.DS_DESCRIPTION);
			User user = null;
			for (CourseAdmin courseAdmin: courseAdmins) {
				CourseAdminRpt rept = new CourseAdminRpt(courseAdmin);
				try {
					user = dataFetcher.getUser(settings, token, courseAdmin.getNetid());
				}catch(Exception ex) {
					rept.addError(ex.toString());
					continue;
				}
				//role: TA or BUILDER
				String role = courseAdmin.getRole().equals("BUILDER") ? "CourseBuilder":"TeachingAssistant";
				String strm = null;
				try {
					strm = Util.term2Strm(courseAdmin.getTerm());
				} catch(Exception ex) {
					log.error(ex.toString());
				}
				// get the list of courseBatchUids
				List<String> courseBatchUids = getCourseBatchUids(
						courseAdmin.dept, 
						strm,
						courseAdmin.career, 
						courseAdmin.getCrosslisted().equals("Y"));		
				for (String courseBatchUid: courseBatchUids) {
					try {
						String ret = dataFetcher.createCourseMembership(settings, token, user.getUuid(), courseBatchUid, Constants.DS_BATCH_UID, role );
						rept.addSuccess(courseBatchUid);
						courseAdminResults.add(rept);
					}catch(Exception ex) {
						log.debug("updateAllDeptEnrollments: exception on adding course enrollment");
						rept.addError(ex.toString());
						courseAdminResults.add(rept);
					}
				}
			}
		}catch(Exception ex) {
		 log.debug(ex.toString());
		}
		return courseAdminResults;
	}
	
	public void deleteEnrollments(
			String uuid, 
			String dept,
			String semester) throws Exception {
		log.debug("netId=" + uuid);
		log.debug("dept=" + dept);
		log.debug("semester=" + semester);
		Token token = null;
		SettingsModel settings = settingsService.loadSettings();
		try {
			dataFetcher.authorize(settings);
		} catch (Exception ex) {
			log.debug("exception on authorization", ex);
		}
		User user = dataFetcher.getUser(settings, token, uuid);
		log.debug(user.getPuid() + " " + user.getNetid());

		// get the list of courseBatchUids
		List<String> courseBatchUids = getCourseBatchUids(
				dept, 
				semester, 
				"", 
				true);
		for (String courseBatchUid: courseBatchUids) {
			try {
			dataFetcher.deleteCourseMembership(settings, token, uuid, courseBatchUid);
			} catch(Exception ex) {
				
			}
		}	
	}

	private List<String> getCourseBatchUids(
			String dept, 
			String semester, 
			String career, 
			boolean doCrossListed) throws Exception {
		List<String> courseBatchUids = new ArrayList<String>();
		String strm = Util.term2Strm(semester);
		
		JSONArray ret = new JSONArray();
		if ("Graduate".equals(career)) {
			ret = json.getCourseBatchUidsGrad(dept, strm, doCrossListed);		
		} else if (("Undergraduate").equals(career)) {
			ret = json.getCourseBatchUidsUngd(dept, strm,doCrossListed);
		} else {
			// for 'All', no condition needs to be added to the query 
			ret = json.getCourseBatchUids4All(dept, strm,doCrossListed);
		}
		for (int i = 0; i< ret.size(); i++){
			JSONObject o = (JSONObject)ret.get(i);
			courseBatchUids. add((String)o.get("EXTERNAL_COURSE_KEY"));
		}
		return courseBatchUids;
	}


	public Set<String> findAllSubjects(String term) {
		try {
		
			int year = Integer.parseInt(term.substring(term.length()-4));
			term = term.substring(0, term.length()-4);

			if ( "SU".equals(term) ) {
				term = "C";
				year += 1;
			} else if ( "F".equals(term) ) {
				year += 1;
			} else if ( ! "S".equals(term) ) {
				throw new Exception("term part invalid: "+term);
			}
			if ( year < 2000 || year > 3000 ) {
				throw new Exception("year out of range: "+year);
			}
			return findAllSubjects(Integer.toString(year), term);
		} catch(Exception e) {
			try {
				log.warn("user not authorized - bad term spec", e);
				} catch (Exception ex) {
			}
			
			return null;
		}
	}

	private Set<String> findAllSubjects(String year, String term)  {
		Set<String> set = new TreeSet<String>();
		JSONArray ret = new JSONArray();	
		
		try {
			ret = json.findAllSubjects(year, term);
			for(int i = 0; i < ret.size(); i++){
				set.add((String)((JSONObject)ret.get(i)).get("COURSE_SUBJECT"));
			}
		} catch (Exception e) {
				e.printStackTrace();
		}
		return set;
	}

	
	public List<DeptStaffEnrollModel> getCourseAdmins(String term, String subject) {
		log.debug("getCourseAdmins: " + term + "," + subject);
		Map<String, String[]> map = lookupAdmins(term, subject);
		List<DeptStaffEnrollModel> retlist = new ArrayList<DeptStaffEnrollModel>();
		for (Map.Entry<String, String[]> entry : map.entrySet()) {
			DeptStaffEnrollModel dsm = new DeptStaffEnrollModel();
			String[] val = entry.getValue();
			dsm.setNetid(entry.getKey());
			if("TA".equals(val[0])){
				dsm.setRole(Constants.INSTRUCTIONAL_STAFF_ROLE);
			}else {
				dsm.setRole(Constants.COURSE_BUILDER_ROLE);
			}
			dsm.setCareer(val[1]);
			dsm.setCrosslisted(val[2]);
			dsm.setName(val[3]);
			retlist.add(dsm);
		}
		return retlist;
	}
	
	public Map<String,String[]> lookupAdmins(String dept, String term) {
		Map<String,String[]> map = new TreeMap<String,String[]>();
		Token token = null;
		SettingsModel settings = settingsService.loadSettings();
		try {
			token = dataFetcher.authorize(settings);
		} catch (Exception ex) {
			log.debug("exception on authorization", ex);
		}
		List<Ds_deptstaff> staffs = repository.lookupAdmins(dept, term);
		for(Ds_deptstaff s : staffs){
			String name = null;
			try {
				User usr = dataFetcher.getUser(settings, token,s.getNetid()); 
				name = usr.getFamily() + ", " + usr.getGiven();
				}catch (Exception ex){
					log.error("Can't find the user from the system with name " +  s.getNetid());
				}
				map.put(s.getNetid(), new String[]{s.getRole(), s.getCareer(), s.getCrosslisted(), name});
		}
		return map;
	}
	
	public String addUserToDeptLists(
			String dept, 
			String term, 
			String netids, 
			String career, 
			String role,
			String crosslisted) {
		
		SettingsModel settings = settingsService.loadSettings();
		try {
			String[] netidArray = netids.split(",");
			String sRole = null;
			String sCrosslisted = null;
			// trim netids
			int i = 0;
			for(String netid: netidArray) {
				netidArray[i++] = netid.trim();
			} 
			//add to Blackboard
			List<User> added = addEnrollments(
					netidArray,
					dept,
					term,
					career,
					role,
					crosslisted);
			
			//add to local database table DSTAFF_DEPTSTAFF
			for ( User usr : added ) {
				if("TeachingAssistant".equals(role)){
					sRole = "TA";
				}else {
					sRole = "BUILDER";
				}
				if("YES".equals(crosslisted.toUpperCase())){
					sCrosslisted = "Y";
				}else {
					sCrosslisted = "N";
				}
					Ds_deptstaff ds = repository.findSameCourseAdmin(term, dept, usr.getNetid(), sRole, career, sCrosslisted);
				if(ds == null){
					log.debug("can't find the same one");
					Ds_deptstaff staff = new Ds_deptstaff();
					staff.setNetid(usr.getNetid());
					staff.setDept(dept);
					staff.setTerm(term);
					staff.setEmplid(usr.getPuid());
					staff.setCareer(career);
					staff.setRole(sRole);
					staff.setCrosslisted(sCrosslisted);
					staff.setCreated(new Timestamp(System.currentTimeMillis()));
					repository.save(staff);	
				}else {
					log.debug("found the same one" );
					ds.setDisabled(null);
					repository.save(ds);
				}
			}
		}catch(Exception ex){
			log.error("can't proceed add user to dept. list");
		}
		
		return Constants.SUCCESS;
	}

	public void deleteUserFromDeptLists(String term, String dept, String netid) {
			
		log.debug("deleteUserFromDeptLists: " + dept + ", " + term + ", " + netid);
		Ds_deptstaff s = repository.getOneCourseAdmin(term, dept, netid);
		log.debug("record from DB: " + s.toString());
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		s.setDisabled(ts);
		repository.save(s);
	}

	public Set<String> getSubjects(String userName, String term) throws Exception {

		// get the list from ps
		Set<String> depts = getDeptsFromPeoplesoftWs(userName);
		log.debug("depts.size()=" + depts.size());

		// if we got none, then issue an authorization failure
		if (depts.size() == 0) {
			log.debug("auth failure - no depts found for " + userName);
			throw new AuthorizationFailure();
		}

		// see javadoc comment for 'deptsToSubjects' to explain the reason for this call
		Set<String> subjects = deptsToSubjects(depts);
		log.debug("subjects.size()=" + subjects.size());
		
		// remove subjects not found for the given semester
		Set<String> allSubjects = findAllSubjects(term);
		if (allSubjects == null) {
			// if term name is badly formed, then fail
			log.debug("findAllSubjects failed");
			return null;
		}
		
		// remove from depts any not found in allDepts for the given semester
		Set<String> finalSubjects = new TreeSet<String>();
		for (String subject: subjects) {
			if (allSubjects.contains(subject) ) {
				finalSubjects.add(subject);
			}
		}
		log.debug("finalSubjects.size()=" + finalSubjects.size());
		
		return finalSubjects;
	}
	
	public Set<String> getDeptsFromPeoplesoftWs(String userName) {
		Set<String> depts = null;
		
		SettingsModel settings = settingsService.loadSettings();
		final String SOAP_URL		= settings.getAuthEndpoint();// "http://eis305l.princeton.edu:12010/PSIGW/PeopleSoftServiceListeningConnector";
		final String SOAP_ACTION 	= settings.getAuthAction(); 	// "CI_PU_BB_SEARCH_CI_F.V3";
		log.debug("soap_url: " + SOAP_URL + "soap_action: " + SOAP_ACTION);
		OutputStreamWriter osw = null;

		try {
		    String soapMsg = getSoapMessage(userName); // eg. acalcado, abryant
			log.debug(soapMsg);
			
			int len = soapMsg.length();
			log.debug(new Integer(len).toString());

			// create the connection where we're going to send the file.
		    URL url 						= new URL(SOAP_URL);
		    HttpURLConnection connection	= (HttpURLConnection) url.openConnection();

		    // set the appropriate http params
		    connection.setRequestProperty("Content-Length", "" + len);
		    connection.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
		    connection.setRequestProperty("SOAPAction", SOAP_ACTION);
		    connection.setRequestMethod("POST");
		    connection.setDoOutput(true);
		    connection.setDoInput(true);

		    // everything is set up so send the soap msg
		    osw = new OutputStreamWriter(connection.getOutputStream());
		    osw.write(soapMsg);    
		    osw.close();

		    // read the response using jdom sax builder
		    Document document = new SAXBuilder().build(connection.getInputStream());

		    // get depts by parsing the document
		    depts = getDeptsFromXmlDoc(document);
			log.debug("depts.size()=" + depts.size());
			
		} catch (Exception e) {
			log.error(e.toString());
		} finally {
			try {
				if (osw != null) osw.close();
			} catch (Exception e) {
			}
		}
		
		return depts;
	}
	
	private String getSoapMessage(String userName) {
		StringBuffer sb = new StringBuffer();

	  	sb.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://xmlns.oracle.com/Enterprise/Tools/schemas/ns15958.V1'>");
		sb.append("<soapenv:Header/>");
	  	sb.append("<soapenv:Body>");
	  	sb.append("<ns1:Find__CompIntfc__PU_BB_SEARCH_CI>");
        sb.append("<ns1:CAMPUS_ID>" + userName + "</ns1:CAMPUS_ID>");
        sb.append("<ns1:ACAD_ORG></ns1:ACAD_ORG>");
        sb.append("<ns1:ACAD_PLAN></ns1:ACAD_PLAN>");
	  	sb.append("</ns1:Find__CompIntfc__PU_BB_SEARCH_CI>");
	  	sb.append("</soapenv:Body>");
	  	sb.append("</soapenv:Envelope>");

		return sb.toString();
	}
	
	private Set<String> getDeptsFromXmlDoc(Document doc) throws Exception {
	    Set<String> depts = new TreeSet<String>();
	    
	    Iterator<?> iterator = doc.getRootElement().getDescendants(new ElementFilter("ACAD_ORG"));
		
		while(iterator. hasNext()){
			Object obj = iterator.next();
			if (obj instanceof Element) {
				Element element = (Element) obj; 
				String dept = element.getText();
				depts.add(dept);
			} else {
				log.error("obj is not of type Element");
			}
	    }		    
		for (String dept: depts) {
			log.debug(dept);			
		}
		
		return depts;
	}
	private Set<String> deptsToSubjects(Set<String> depts) {
		Set<String> subjects = new TreeSet<String>();
		JSONArray ret = new JSONArray();
		
		//call JsonDAO.getSubjects
		// "select subject from PU_PS.ACTIVE_ACADORG where ACAD_ORG in (" + deptInStr + ")";
		
		try {
			ret = json.getSubjects(depts);
			for(int i = 0; i < ret.size(); i++){
				JSONObject jsonObj = (JSONObject)ret.get(i);
				subjects.add((String)jsonObj.get("SUBJECT"));
			}
		} catch (Exception e) {
				e.printStackTrace();
		}
		return subjects;
	}
}
