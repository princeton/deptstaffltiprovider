package edu.princeton.deptstafflti.service.impl;

import edu.princeton.deptstafflti.config.DeptStaffProperties;
import edu.princeton.deptstafflti.exception.GenericLtiException;
import edu.princeton.deptstafflti.model.SettingsModel;
import edu.princeton.deptstafflti.service.SettingsService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.scope.refresh.RefreshScopeRefreshedEvent;
import org.springframework.context.ApplicationListener;

import edu.princeton.deptstafflti.Constants;
public class SettingsServiceImpl implements SettingsService,InitializingBean, 
DisposableBean, ApplicationListener<RefreshScopeRefreshedEvent> {
	
	@Autowired DeptStaffProperties properties;
	
	private static Logger log = LoggerFactory.getLogger(SettingsServiceImpl.class);
	private volatile static RefreshScopeRefreshedEvent event;  
	private SettingsModel settingsModel = null;
	
	public void setSettingsModel(SettingsModel sets){
		this.settingsModel = sets;
	}
	public SettingsModel getSettingsModel() {
		return this.settingsModel;
	}
	@Override 
	  public void onApplicationEvent(RefreshScopeRefreshedEvent e) { 
	   event = e; 
	  } 
    
    public void saveSettings(SettingsModel model) throws GenericLtiException {
        // Write settings to properties file
    	FileOutputStream fos = null;
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    	LocalDateTime now = LocalDateTime.now();
        Properties properties = new Properties();
        properties.put("learnUrl", model.getLearnUrl() == null ? "http://localhost:9876" : model.getLearnUrl());
        properties.put("key", model.getKey());
        properties.put("restKey", model.getRestKey());
        properties.put("secret", model.getSecret());
        properties.put("restSecret", model.getRestSecret());
        properties.put("authEndpoint", model.getAuthEndpoint());
        properties.put("authAction", model.getAuthAction());
        properties.put("authMethod", model.getAuthMethod());
        properties.put("authParamId", model.getAuthParamId());
        properties.put("authParamOrg", model.getAuthParamOrg());
        properties.put("emailTo", model.getEmailTo());
        properties.put("emailFrom", model.getEmailFrom());
        properties.put("scheduleStart", model.getScheduleStart() == null ? Constants.SCHEDULE_START_DEFAULT : model.getScheduleStart());
        properties.put("schedulerPeriod", model.getSchedulerPeriod() == null ? Constants.SCHEDULER_PERIOD_DEFAULT : model.getSchedulerPeriod());
        properties.put("lastUpdated", now.format(formatter));
        properties.put("webUrl", model.getWebUrl());
        properties.put("webSvcKey", model.getWebSvcKey());
        properties.put("currentSemester", model.getCurrentSemester());
        
        try {
        	  File f = new File(Constants.PROPERTY_FILE_ABSOLUTE_PATH);
              fos = new FileOutputStream(f);
        	//fos = new FileOutputStream(getClass().getResource(Constants.PROPERTY_FILE_DIR).getFile());
            properties.store(fos, "storing");
            fos.close();
            fos = null;
            log.debug("saved in the properties file");
        } catch (IOException e) {
            throw new GenericLtiException (e);
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                    fos = null;
                } catch (Exception ex) {
                    throw new GenericLtiException (ex);
                }
            }
        }
    }

	public SettingsModel loadSettings()  {
		SettingsModel s = new SettingsModel();
		s.setLearnUrl(properties.getLearnUrl());
		s.setKey(properties.getKey());
		s.setRestKey(properties.getRestKey());
		s.setRestSecret(properties.getRestSecret());
		s.setSecret(properties.getSecret());
		s.setAuthAction(properties.getAuthAction());
		s.setAuthEndpoint(properties.getAuthEndpoint());
		s.setAuthMethod(properties.getAuthMethod());
		s.setAuthParamId(properties.getAuthParamId());
		s.setAuthParamOrg(properties.getAuthParamOrg());
		s.setEmailTo(properties.getEmailTo());
		s.setEmailFrom(properties.getEmailFrom());
		s.setScheduleStart(properties.getScheduleStart());
		s.setSchedulerPeriod(properties.getSchedulerPeriod());
		s.setWebUrl(properties.getWebUrl());
		s.setWebSvcKey(properties.getWebSvcKey());
		s.setCurrentSemester(properties.getCurrentSemester());
		s.setLastUpdated(properties.getLastUpdated());
		this.settingsModel = s;
		return settingsModel;
	}

	@Override
	public void destroy() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		
	}
}