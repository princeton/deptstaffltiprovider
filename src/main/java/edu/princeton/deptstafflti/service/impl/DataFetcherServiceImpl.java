package edu.princeton.deptstafflti.service.impl;


import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import edu.princeton.deptstafflti.Constants;
import edu.princeton.deptstafflti.rest.RestConstants;
import edu.princeton.deptstafflti.rest.RestRequest;
import edu.princeton.deptstafflti.service.DataFetcherService;
import edu.princeton.deptstafflti.data.DataSource;
import edu.princeton.deptstafflti.data.Token;
import edu.princeton.deptstafflti.data.User;
import edu.princeton.deptstafflti.exception.GenericRestException;
import edu.princeton.deptstafflti.model.SettingsModel;
import edu.princeton.deptstafflti.service.impl.DataFetcherServiceImpl;

@Service
public class DataFetcherServiceImpl implements DataFetcherService {
	
	private static final Logger logger = LoggerFactory
			.getLogger(DataFetcherServiceImpl.class);
	
	@Cacheable("accessToken")
    public Token authorize (SettingsModel settingsModel) throws GenericRestException {
        RestTemplate restTemplate = new RestTemplate();
        URI uri = null;
        Token token = null;

        try {
            uri = new URI(settingsModel.getLearnUrl() + Constants.AUTH_PATH );
           
            String auth = settingsModel.getRestKey() + ":" + settingsModel.getRestSecret();
            
            String hash = Base64.getEncoder().encodeToString(auth.getBytes());
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", "Basic " + hash);
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            HttpEntity<String> request = new HttpEntity<String>("grant_type=client_credentials",headers);
            
            ResponseEntity<Token> response = restTemplate.exchange(uri, HttpMethod.POST, request, Token.class);
            token = response.getBody();
            if (token != null){
            	logger.debug("the access_token is " + token.getAccess_token());
            }else {
            	logger.debug("token is null");
            }
        } catch (URISyntaxException e) {
            throw new GenericRestException (e);
        }
        return token;
    }
	

	public User getUser(SettingsModel settings, Token token, String netid) throws Exception {
		User user = new User();
		RestTemplate restTemplate = new RestTemplate();
        URI uri = null;
        try {
            uri = new URI(settings.getLearnUrl() + Constants.USERS_PATH + "/userName:" + netid);
            
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", "Bearer " + token.getAccess_token());
            headers.setContentType(MediaType.APPLICATION_JSON);
	        HttpEntity<String> request = new HttpEntity<String>(new String(), headers);
            ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.GET, request, String.class);
            //logger.debug("response.getString(): " + response.toString());
            String str = response.getBody();
            int code = response.getStatusCodeValue();
            //logger.debug("str: " + str + ", code: " + code);
            JSONParser parser = new JSONParser();
	        JSONObject first = (JSONObject) parser.parse(str);
	        user.setFamily((String)((JSONObject)first.get("name")).get("family"));
	        user.setGiven((String)((JSONObject)first.get("name")).get("given"));
	        user.setNetid(netid);
	        user.setPuid((String)first.get("studentId"));
	        user.setUuid((String)first.get("uuid"));
	        
        } catch (URISyntaxException e) {
            throw new GenericRestException (e);
        } catch (HttpClientErrorException ex){
        	int code = ex.getRawStatusCode();
        	if(code == 404){
        		logger.debug("can't find netid: " + netid + " in the system.");
        		return null;
        	}
        }
        return user;
		
	}

	public List<User> getUsers(SettingsModel settings, Token token, String[] netIds) throws Exception {
		List<User> users = new ArrayList<User>();
		
        for (String netid : netIds) {
        	try {
        		User user = getUser(settings, token, netid); 
        		if(user != null) {
        			users.add(user);
        		}
        	} catch (URISyntaxException e) {
        		throw new GenericRestException (e);
        	}
        }
        return users;
	}
	public List<String> validateUsers(SettingsModel settings, Token token, String[] netIds){
		List<String> ids = new ArrayList<String>();
		ids.clear();
		for(String netid : netIds){
			try {
				logger.debug("the netid is : " + netid.trim());
				User user = getUser(settings, token, netid.trim());
				if(user == null) {
					ids.add(netid);
				}
			}catch (Exception ex){
				logger.debug("validateUsers: " + ex.getMessage());
				ids.add(netid);	
			}
		}
		return ids;
	}
	/**
	 * Note: for inserting new records, if no dataSource is supplied the default is 
	 *       '2	0	SYSTEM	System data source used for associating records that are created via web browser.' 
	 *
	 * @return the deptstafflti dataSource - if it does not exist a new one will be created
	 * @throws Exception
	 */
	public DataSource getDataSource(SettingsModel settings, Token token, String batchuid, String description) throws Exception {
		DataSource ds = new DataSource();
		RestTemplate restTemplate = new RestTemplate();
        URI uri = null;
        logger.debug("batchuid: " + batchuid);
        try {
            uri = new URI(settings.getLearnUrl() + Constants.DATASOURCE_PATH  + "/externalId:" + batchuid);
            
            logger.debug("uri: " + uri.getPath());
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", "Bearer " + token.getAccess_token());
            headers.setContentType(MediaType.APPLICATION_JSON);
	        HttpEntity<String> request = new HttpEntity<String>(new String(), headers);
            ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.GET, request, String.class);
            String str = response.getBody();
            logger.debug("str: " + str);
            HttpStatus returncode = response.getStatusCode();
            if(returncode == HttpStatus.NOT_FOUND){
            	try {
            	//create the new ds
            	addDataSource(settings, token, batchuid, description);
            	}catch(Exception ex) {
            		logger.debug("can't add new datasource key to system");
            		ex.printStackTrace();
            	}
            }else {
            	JSONParser parser = new JSONParser();
     	        JSONObject first = (JSONObject) parser.parse(str);
     	        ds.setBatchuid((String)first.get("externalId"));
     	        ds.setDescription((String)first.get("description"));
            }
        }catch (Exception ex) {
        	logger.debug("exception when try to retrive the datasource");
        	ex.printStackTrace();
        	
        }
        return ds;
	}
	
	public String addDataSource(SettingsModel settings, Token token, String batchuid, String description) throws Exception {
		return(RestRequest.sendRequest(RestConstants.DATASOURCE_PATH, HttpMethod.POST, token.getAccess_token(), getDSBody(batchuid, description)));
	}
	
	private String getDSBody(String id, String description) {
		
		ObjectMapper objMapper = new ObjectMapper();
		JsonNode datasource = objMapper.createObjectNode();
		((ObjectNode) datasource).put("externalId", id);
		((ObjectNode) datasource).put("description", description);

		String body = "";
		try {
			body = objMapper.writeValueAsString(datasource);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return(body);
	}
   
    public String createCourseMembership(SettingsModel settings, Token token, String userUuid, String courseExternalId, String datasource, String courseRole) throws Exception {
    	logger.debug("course: " + courseExternalId + ", user: " + userUuid + ", course role: " + courseRole);
    	return(RestRequest.sendRequest(RestConstants.COURSE_PATH + "/externalId:" + courseExternalId + "/users/uuid:" + userUuid.trim(), HttpMethod.PUT, token.getAccess_token(), getMembershipBody(courseRole))); 
    }
    
    public String createCourseMembershipByPUId(SettingsModel settings, Token token, String puid, String courseExternalId, String datasource, String courseRole) throws Exception {
    	logger.debug("course: " + courseExternalId + ", user: " + puid + ", course role: " + courseRole);
    	return(RestRequest.sendRequest(RestConstants.COURSE_PATH + "/externalId:" + courseExternalId + "/users/externalId:" + puid.trim(), HttpMethod.PUT, token.getAccess_token(), getMembershipBody(courseRole))); 
    }
    

    private String getMembershipBody(String role) {
    	ObjectMapper objMapper = new ObjectMapper();  
        ObjectNode membership = objMapper.createObjectNode();  
        //membership.put("dataSourceId", RestConstants.DATASOURCE_ID);  
        ObjectNode availability = membership.putObject("availability");  
        availability.put("available", "Yes");  
        membership.put("courseRoleId", role);  
     
        String body = "";  
        try {  
             body = objMapper.writeValueAsString(membership);  
        } catch (JsonProcessingException e) {  
             e.printStackTrace();  
        }  
     
        return(body);  
   }  
    public String deleteCourseMembership(SettingsModel settings, Token token, String uuid, String courseBatchUid ) throws Exception {
    	   
    	return(RestRequest.sendRequest(RestConstants.COURSE_PATH + "/externalId:" + courseBatchUid + "/users/uuid:"+ uuid.trim(), HttpMethod.DELETE, token.getAccess_token(), ""));
    
    }
    
    /*
    public void deleteCourseMembership(SettingsModel settings, Token token, String netid, String courseBatchUid ) throws Exception {
    	RestTemplate restTemplate = new RestTemplate();
     	URI uri = null;
     	
        try {
        	uri = new URI(settings.getLearnUrl() + Constants.COURSEMEMBERSHIP_PATH 
        			+ "/externalId:" + courseBatchUid + "/users" + "/externalId:" + netid );
        	HttpHeaders headers = new HttpHeaders();
        	headers = new HttpHeaders();
        	headers.add("Authorization", "Bearer " + token.getAccess_token());
        	headers.setContentType(MediaType.APPLICATION_JSON);
        	HttpEntity<String> request = new HttpEntity<String>(new String(), headers);
        	request = new HttpEntity<String>(new String(), headers);
        	ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.DELETE, request, String.class);
        	
        }catch (Exception ex) {   
        	throw new GenericRestException (ex);
        }
    }
    */
}
