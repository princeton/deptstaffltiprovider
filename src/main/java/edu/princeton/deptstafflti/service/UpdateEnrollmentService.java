package edu.princeton.deptstafflti.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.princeton.deptstafflti.data.CourseAdminRpt;
import edu.princeton.deptstafflti.model.SettingsModel;
import edu.princeton.deptstafflti.scheduler.Emailer;

@Service
public class UpdateEnrollmentService   {

	public Logger log = LoggerFactory.getLogger(UpdateEnrollmentService.class);
	final String DATE_FORMAT    = "yyyy/MM/dd HH:mm:ss";
	@Autowired
	DeptStaff deptStaff;
	
	public void update(SettingsService s)  {
		    SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		    String emailFrom = s.loadSettings().getEmailFrom();
		    String emailTo= s.loadSettings().getEmailTo();
		    String emailSubject = getClass().getSimpleName();
		    String emailText = "Started at "
		            + dateFormat.format(new Date()) + ".\n";
		        // Flag indicating whether to send confirmation email or not.
		    
		    boolean canSendEmail = (emailTo != null && emailFrom != null && emailTo.length() > 0 && emailFrom.length() > 0);
		 
		    log.debug("calling context.updateAllDeptEnrollments()");
		    String result = null;
		    try {
		    List<CourseAdminRpt> courseAdmins = deptStaff.updateAllDeptEnrollments();
		    int numProcessed = courseAdmins.size();
		    result = "call to context.updateAllDeptEnrollments() done: " + numProcessed + " courseAdmins processed. Here are the details : " + getDetails(courseAdmins);
		    log.debug(result);
		      emailText += "Ended at "
		                + dateFormat.format(new Date()) + ".\n\n" + result;
		    if (canSendEmail) {
		            new Emailer().sendEmail(emailFrom, emailSubject, emailText, emailTo);
		     }
		    } catch(Exception e) {
		    	log.error("Can't complete update enrollment service" + e.toString());
		    }
		    String logText = canSendEmail == true ? result + " email is sent." : result;
		    log.debug(logText);
		}
		
		private String getDetails(List<CourseAdminRpt> courseAdminResults) {
	    	if (courseAdminResults == null || courseAdminResults.size() == 0) {
	    		return "no admin record has been processed this time.";
	    	}
	    	StringBuffer strBuff = new StringBuffer();
	    	for (CourseAdminRpt courseAdmin: courseAdminResults) {
				List<String> successes = courseAdmin.getSuccesses();
				List<String> errors = courseAdmin.getErrors();
				
				if (successes.size() > 0 || errors.size() > 0) {
					strBuff.append(courseAdmin.getDescription() + "\n");
					if (successes.size() > 0) {
						strBuff.append("Successfully added to: " + "\n");
						for (String success: successes) {
							strBuff.append(success + "\n");
						}
					}
					if (errors.size() > 0) {
						strBuff.append("Errors: " + "\n");
						for (String error: errors) {
							strBuff.append(error + "\n");
						}
					}
				}
			}
	    	return strBuff.toString();
	    }
	    
		}


