package edu.princeton.deptstafflti.exception;

public class GenericRestException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	public GenericRestException(Exception ex) {
        super (ex);
    }
}