package edu.princeton.deptstafflti.exception;

public class GenericLtiException extends Exception {
    
	private static final long serialVersionUID = 1L;

	public GenericLtiException (Exception ex) {
        super (ex);        
    }
}