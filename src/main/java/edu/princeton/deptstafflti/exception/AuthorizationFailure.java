package edu.princeton.deptstafflti.exception;

public class AuthorizationFailure extends Exception {
		private static final long serialVersionUID = 1L;
		public AuthorizationFailure() {
			super("No allowed departments for user.");
		}
	}